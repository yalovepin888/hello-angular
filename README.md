# README Tutorial
Hello Angular

* install nodejs with npm(node package manager)
    * https://nodejs.org/en/
* install angular cli
* > npm i -g angular/cli
* download visual studio code(vscode)
    * https://code.visualstudio.com/
    * add to the path
    ![](https://i.imgur.com/gmqJfcB.png)

* install the angualr plugin into vscode
    ![](https://i.imgur.com/UA3GsH7.png)

* create new project by using angular/cli
    * open command line tool
    * > ng new hello-angular
    
    ![install package](https://i.imgur.com/TT4mluT.png)
    * > cd hello-angular
    * > code .
    * it will open the editor
* compile the angular project
    * > ng serve --open

* Official Tutorial
    * https://angular.io/tutorial



# HelloAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
